//
//  mapViewVC.swift
//  foodTruck
//
//  Created by Canyon Duncan on 10/28/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import MapKit
import Parse
import ParseUI
import CollectionPickerView
//import AVFoundation


class mapViewVC: UIViewController {
    
    fileprivate lazy var locationManager: CLLocationManager = CLLocationManager()
    
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    let fadeView:UIView = UIView()
    
    var stateController : StateController?
    //var pickerView = CollectionPickerView()
    
   // @IBOutlet weak var collectionViewFilter: UICollectionView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mapView: MKMapView!

    @IBOutlet weak var routeDetailLabel: UILabel!
    @IBOutlet weak var searchResultsWrapperView: UIVisualEffectView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    
    @IBOutlet weak var pickerView: CollectionPickerView!
    //@IBOutlet weak var pickerView: CollectionPickerView!
   
    
    let font = UIFont(name: "HelveticaNeue-Light", size: 12)!
    let highlightedFont = UIFont(name: "HelveticaNeue", size: 12)!
    var selectedIndex = 0

    
    var companyName = String()
    var eventDetails = UIView()
    var detailTitle = UILabel(frame: CGRect.zero)
    var timeDateLabel = UILabel(frame: CGRect.zero)
    var getDirections = UIButton(frame: CGRect.zero)
    var imageView = UIImageView(frame: CGRect.zero)
    var curEventCoordinate = CLLocationCoordinate2D()
    var curEventName = String()
    var currentLocation = PFGeoPoint()
    var vendors: [PFObject] = []
    var vendorName: [String] = []
    var filteredName: [String] = []
    var isSearching = false
    
    // Infinite Collection View
    
    let infiniteSize = 10000

    
    override func viewWillAppear(_ animated: Bool) {
        reloadInputViews()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        self.stateController?.getVendors()
        //pickerView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.9)
        pickerView.backgroundColor = UIColor.clear
        pickerView.dataSource = self
        pickerView.delegate = self
        
        pickerView.collectionView.register(
            CollectionPickerViewCell.self,
            forCellWithReuseIdentifier: NSStringFromClass(CollectionPickerViewCell.self))
        
        //pickerView.cellSize =
//        pickerView.isFlat = true
        pickerView.reloadData()
        fadeView.frame = self.view.frame
        fadeView.backgroundColor = UIColor.black
        fadeView.alpha = 0.7
        
        self.view.addSubview(fadeView)
        
        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating()
        
        self.hideKeyboardWhenTappedAround()
        
        mapView.delegate = self
        //mapView.userTrackingMode = .followWithHeading
        mapView.mapType = .standard
        mapView.showsBuildings = true
        //mapView.showsScale = true
        //mapView.showsCompass = true
        mapView.showsUserLocation = true
        
         searchResultsWrapperView.isHidden = true
        

        //let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout // casting is required because UICollectionViewLayout doesn't offer header pin. Its feature of UICollectionViewFlowLayout
       // layout?.sectionHeadersPinToVisibleBounds = true
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        //collectionViewFilter.delegate = self
        //collectionViewFilter.dataSource = self
        
        self.collectionView.register(HeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader , withReuseIdentifier: "header")

        
        if CLLocationManager.locationServicesEnabled() {
            self.stateController?.getCurrentLocation()
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getDataUpdate), name: NSNotification.Name(rawValue: "dataModelDidUpdateNotification"), object: nil)

        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
    }
    
    @objc private func getDataUpdate() {
        self.collectionView.reloadData()
        //self.mapView.addAnnotations((self.stateController?.mapAnnotations[0])!);
        //self.mapView.addAnnotations((self.stateController?.mapAnnotations[1])!);
        //self.mapView.addAnnotations((self.stateController?.mapAnnotations[2])!);
        //for anno in (self.stateController?.mapAnnotations)! {
        self.mapView.addAnnotations((self.stateController?.mapAnnotations[0])!)
        //}
        mapView.fitAll()
        pickerView.reloadData()
        fadeView.removeFromSuperview()
        self.activityView.stopAnimating()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Infinite Scroll
        
//        let midIndexPath = IndexPath(row: infiniteSize / 2, section: 0)
//        collectionView.scrollToItem(at: midIndexPath,
//                                         at: .centeredHorizontally,
//                                         animated: false)
        
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "dataModelDidUpdateNotification"), object: self)
    }
    
   
}

extension mapViewVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    ///CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(collectionView == self.collectionView){
                //return 3
                return 1
        }
        else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.collectionView){
            //return (self.stateController?.mapAnnotations[section].count)!
            
            if !((self.stateController?.mapAnnotations.isEmpty)!) {
                return (self.stateController?.mapAnnotations[selectedIndex].count)!
            }
            else{
                return 0
            }
            
        }
        else {
            return (self.stateController?.mapAnnotations.count)!
        }
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == self.collectionView){
            //let id = self.stateController?.mapAnnotations[indexPath.section][indexPath.item].companyID
            let id = self.stateController?.mapAnnotations[selectedIndex][indexPath.item].companyID
            self.stateController?.selectedTruck = self.stateController?.foodTruck[id!]
            performSegue(withIdentifier: "showViewCompany", sender: self)
            
        }
        else{
            //print("hello world")
            
            mapView.removeAnnotations( mapView.annotations.filter { $0 !== mapView.userLocation } )
            
            self.mapView.addAnnotations((self.stateController?.mapAnnotations[indexPath.row])!);
            self.selectedIndex = indexPath.row
            self.collectionView.reloadData()
            mapView.fitAll()
        }
        
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showViewCompany", let destination = segue.destination as? ViewCompanyVC {
            
            if(destination.stateController == nil){
                destination.stateController = self.stateController
            }
            destination.companyID = self.stateController?.selectedTruck?.objectId
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == self.collectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionCell
            let anno : FoodTruckLocation = (self.stateController?.mapAnnotations[selectedIndex][indexPath.row])!
            
            cell.label?.text = anno.title
            cell.distanceLabel.text = anno.eventStartEndTime

            
            if let imageFile : PFFile = anno.image {
                imageFile.getDataInBackground(block: { (data, error) in
                    if error == nil {
                        DispatchQueue.main.async {
                            // Async main thread
                            
                            let image = UIImage(data: data!)
                            cell.imageView.image = image
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            }
            return cell
        }
        else{
            //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filtercell", for: indexPath) as! CollectionFilterCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(CollectionPickerViewCell.self), for: indexPath) as! CollectionPickerViewCell
           
            
            var title = ""
            //let title = titles[indexPath.item]
            //if let anno : FoodTruckLocation = (self.stateController?.mapAnnotations[indexPath.row][0])! {
              // cell.label.text = anno.weekday
            //}
            if(!(self.stateController?.mapAnnotations[indexPath.row].isEmpty)!){
                let dateOfEvent = self.stateController?.mapAnnotations[indexPath.row].first?.date
                if(Calendar.current.isDateInToday(dateOfEvent!)){
                    title = "Today"
                }
                else if(Calendar.current.isDateInTomorrow(dateOfEvent!)){
                    title = "Tomorrow"
                }
                else {
                    
                    title = Calendar.current.component(.weekday, from: dateOfEvent!).getWeekDay()
                }
            }
            
            
            

            cell.label.text = title
            //cell.label.font = font
            //cell.font = font
            //cell.highlightedFont = highlightedFont
            //cell.label.bounds = CGRect(origin: CGPoint.zero, size: sizeForString(title as NSString))
            return cell
        }
    
    
    
        
        
    }
    
    
    
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let reusableView = self.collectionView!.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! HeaderView
//        if(indexPath.section == 0){
//            reusableView.titleLabel.text = "Today"
//        }
//        else if (indexPath.section == 1){
//            reusableView.titleLabel.text = "Tomorrow"
//        }
//        else{
//            let weekday = Calendar.current.component(.weekday, from: Date().tomorrow.tomorrow)
//            reusableView.titleLabel.text = weekday.getWeekDay()
//        }
//        
//        
//        return reusableView
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
//        return CGSize(width: CGFloat(collectionView.frame.size.width / 1.5), height: CGFloat(collectionView.frame.size.height)) // you can change here
//    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        self.findCenterIndex()
        
        
        
    }
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        //caused by code
        //print("SCROLL scrollViewDidEndScrollingAnimation")
        self.findCenterIndex()
    }
    
}




extension MKMapView {
    /// when we call this function, we have already added the annotations to the map, and just want all of them to be displayed.
    func fitAll() {
        var zoomRect            = MKMapRectNull;
        for annotation in annotations {
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect       = MKMapRectMake(annotationPoint.x, annotationPoint.y, 10000, 10000);
            zoomRect            = MKMapRectUnion(zoomRect, pointRect);
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(150, 100, 150, 100), animated: true)
    }
    
    /// we call this function and give it the annotations we want added to the map. we display the annotations if necessary
    func fitAll(in annotations: [MKAnnotation], andShow show: Bool) {
        var zoomRect:MKMapRect  = MKMapRectNull
        
        for annotation in annotations {
            let aPoint          = MKMapPointForCoordinate(annotation.coordinate)
            let rect            = MKMapRectMake(aPoint.x, aPoint.y, 0.1, 0.1)
            
            if MKMapRectIsNull(zoomRect) {
                zoomRect = rect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, rect)
            }
        }
        if(show) {
            addAnnotations(annotations)
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
}

extension mapViewVC: MKMapViewDelegate {
    
    
    func showAnnotationTitle(index: Int) {
        self.mapView.selectAnnotation(self.mapView.annotations[index], animated: true)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            
            return nil;
        }else{
            let pinIdent = "Pin";
            var pinView: MKAnnotationView;
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation;
                dequeuedView.animatesDrop = true
                pinView = dequeuedView;
            
                
            }else{
                let curLocation = CLLocation(latitude: (self.stateController?.currentLocation?.latitude)!, longitude: (self.stateController?.currentLocation?.longitude)!)
                let coords = CLLocation(latitude: (annotation.coordinate.latitude), longitude: (annotation.coordinate.longitude))
    
                let distanceInMeters = curLocation.distance(from: coords)
                let distanceInMiles = String(format: "%.1f", distanceInMeters/1609.344)
                let distanceString = distanceInMiles + " miles"
                //annotation.
                let anno = annotation as! FoodTruckLocation
                anno.setSubtitle(sub: distanceString)
                
                
                pinView = MKAnnotationView(annotation: anno, reuseIdentifier: pinIdent);
                
                //pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdent)
                //pinView.animatesDrop = true
                pinView.canShowCallout = true
                //pinView.image = UIImage(named: "TruckAnno")
                
                pinView.calloutOffset = CGPoint(x: -5, y: 0)
                
                let button = UIButton(type: .custom)
                pinView.rightCalloutAccessoryView = button
                
                button.frame = CGRect(origin: .zero, size: CGSize(width: 60, height: 20))
                button.setTitle("Let's Go!", for: .normal)
                button.titleLabel?.adjustsFontSizeToFitWidth = true
                button.setTitleColor( #colorLiteral(red: 0.7157162119, green: 0.06374814431, blue: 0.1042156499, alpha: 1), for: .normal)
                //                button.layer.setValue(annotation, forKey: mapViewVC.AnnotationKeyPath)
                button.addTarget(self, action: #selector(openMapForPlace(_:)), for: .touchUpInside)
                
                
            }
            // Resize image
            
            let transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
            
            let pinImage = UIImage(named: "TruckAnno")
            //            pinView.transform = transform
            pinView.image = pinImage
            
            return pinView;
        }
    }
    
   
    
    
    
    
    @objc func openMapForPlace(_ sender: UIButton) {
        
        let latitude: CLLocationDegrees = (self.stateController?.selectedEvent?.coordinate.latitude)!
        let longitude: CLLocationDegrees = (self.stateController?.selectedEvent?.coordinate.longitude)!
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.stateController?.selectedEvent?.title
        mapItem.openInMaps(launchOptions: options)
    }
    
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//
//        let renderer = MKPolylineRenderer(overlay: overlay)
//        renderer.strokeColor = #colorLiteral(red: 0.7157162119, green: 0.06374814431, blue: 0.1042156499, alpha: 1)
//        renderer.lineWidth = 2.0
//        let mapRect = MKPolygon(points: renderer.polyline.points(), count: renderer.polyline.pointCount)
//        mapView.setVisibleMapRect(mapRect.boundingMapRect, edgePadding: UIEdgeInsets(top: 160.0,left: 90.0,bottom: 200.0,right: 30.0), animated: true)
//        return renderer
//
//    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        

        let anno = view.annotation as? FoodTruckLocation
        self.stateController?.selectedEvent = anno
        
        if(anno != nil){
            //let outer : Int = (stateController?.mapAnnotations.index(of: [anno!]))!
            let inner : Int = (stateController?.mapAnnotations[selectedIndex].index(of: anno!))!
            self.collectionView.scrollToItem(at: [0, inner], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }

        
       
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
//            let company = (view.annotation?.title)!
//            let curLocation = CLLocation(latitude: self.currentLocation.latitude, longitude: self.currentLocation.longitude)
//            let coords = CLLocation(latitude: (view.annotation?.coordinate.latitude)!, longitude: (view.annotation?.coordinate.longitude)!)
//
//            self.curEventCoordinate.latitude = (view.annotation?.coordinate.latitude)!
//            self.curEventCoordinate.longitude = (view.annotation?.coordinate.longitude)!
//            self.curEventName = ((view.annotation?.title)!)!
//
//            let distanceInMeters = curLocation.distance(from: coords)
//            let distanceInMiles = String(format: "%.1f", distanceInMeters/1609.344)
//            let distanceString = distanceInMiles + " miles"
//
            //self.getDirections.setTitle(distanceString, for: UIControlState.normal)
            //self.detailTitle.text = company
            //self.timeDateLabel.text = distanceString
            //            self.showDetailView()
            
            //turnByTurn()
            
           // reloadInputViews()
        }
    }
    
    func zoomMapaFitAnnotations() {
        
        var zoomRect = MKMapRectNull
        for annotation in self.mapView.annotations {
            
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
            
            if (MKMapRectIsNull(zoomRect)) {
                zoomRect = pointRect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, pointRect)
            }
        }
        self.mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(0, 0, 0, 0), animated: true)
        
    }
    
    func findCenterIndex() {
        //reset()
        
        //let center = self.wrapperView.convert(self.collectionView.center, to: self.collectionView)
        let center = self.view.convert(self.collectionView.center, to: self.collectionView)
        let index = collectionView!.indexPathForItem(at: center)
        //let index = collectionView!.indexPathForItem(at: self.collectionView.center)
        if index != nil {
            
            self.collectionView.scrollToItem(at: [(index?.section)!, (index?.row)!], at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            let anno = self.stateController?.mapAnnotations[pickerView.selectedIndex][(index?.row)!]
           
            
            
            self.mapView.selectAnnotation(anno!, animated: true)
            let myCoordinate = anno?.coordinate
            let coordinateRegion2 = MKCoordinateRegionMakeWithDistance(myCoordinate!, 5000, 5000)
            mapView.setRegion(coordinateRegion2, animated: true)
            
            
        }
        
       
    }
    
//    func reset() {
//
//        DispatchQueue.main.async {
//
//            self.mapView.removeOverlays(self.mapView.overlays)
//            let overlays = self.mapView.overlays
//            self.mapView.removeOverlays(overlays)
//
//            //self.routeDetailLabel.text = ""
//            //            let annotations = self.mapView.annotations //.filter { !$0.isKind(of: MKPlacemark.self) }
//            //            self.mapView.removeAnnotations(annotations)
//
//        }
//    }
}

// MARK: - UISearchBarDelegate

extension mapViewVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            
            tableView.reloadData()
        }
        else{
            isSearching = true
            //self.stateController?.filteredName = (self.stateController?.vendors.filter({$0.contains(searchBar.text!)}))!
            self.stateController?.filteredName = (self.stateController?.vendors.filter({$0.name.contains(searchBar.text!)}))!
            tableView.reloadData()
        }
        
        searchVendors(searchText: searchText)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchVendors(searchText: searchBar.text!)
    }
}

// MARK: - MKLocalSearchCompleterDelegate

extension mapViewVC: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource

extension mapViewVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            return (self.stateController?.filteredName.count)!
        }
        
        return (self.stateController?.vendors.count)! //self.vendors.count  // 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as UITableViewCell!
        if isSearching{
            cell.textLabel?.text = self.stateController?.filteredName[indexPath.row].name
        }
        else{
            cell.textLabel?.text = self.stateController?.vendors[indexPath.row].name
        }
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension mapViewVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = UIColor.clear
        //cell.textLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.textLabel?.textColor = UIColor.black
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let indexPath = tableView.indexPathForSelectedRow!
        //let currentCell = tableView.cellForRow(at: indexPath)!
        if isSearching {
            let id = self.stateController?.filteredName[indexPath.row].id
            self.stateController?.selectedTruck = self.stateController?.foodTruck[id!]
                //self.stateController?.selectedTruck = self.stateController?.filteredName
        }
        else {
            let id = self.stateController?.vendors[indexPath.row].id
            self.stateController?.selectedTruck = self.stateController?.foodTruck[id!]
        }
        
        //self.companyName = (currentCell.textLabel?.text)!
        
        performSegue(withIdentifier: "showViewCompany", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
        searchResultsWrapperView.isHidden = true
        searchBar.resignFirstResponder()
        
//                var companyTitle = String()
//                tableView.deselectRow(at: indexPath, animated: true)
//                companyTitle = (tableView.cellForRow(at: indexPath)?.textLabel?.text)!
//                for vendor in vendors {
//                    if vendor["FoodTruckCompany"] as! String == companyTitle{
//                        selectedCompanyID = vendor.objectId
//                        break
//                    }
//                }
//                if(selectedCompanyID != nil){
//                    performSegue(withIdentifier: "showViewCompany", sender: self)
//                }
    }
}


extension mapViewVC {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(mapViewVC.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    fileprivate func searchVendors(searchText: String) {
                searchResultsWrapperView.isHidden = searchText.isEmpty
//                localSearchCompleter.queryFragment = searchText
    }
}






