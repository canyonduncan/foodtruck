//
//  StateController.swift
//  foodTruck
//
//  Created by Canyon Duncan on 7/12/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import Foundation

class StateController {
    
    
    var currentLocation : PFGeoPoint?
    var distance : Double = 100.00
    var stillfetching = false
    
    //var vendors: [String] = []
    //var filteredName: [String] = []
    var vendors : [Vendor] = []
    var filteredName : [Vendor] = []
    
    var filteredFoodTruck : [String ] = [  ]
    var foodTruck : [String : PFUser] = [:]
    var mapAnnotations : [[FoodTruckLocation]] = [ ] {
        didSet{
            if(!stillfetching){
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataModelDidUpdateNotification"), object: nil)
            }
            
        }
        
    }
    
    
    var selectedEvent: FoodTruckLocation?
    
    var selectedTruck : PFUser? {
        didSet{
            
        }
    }
    
    
    init() {
        //self.getCurrentLocation()
        //print("i am in the state controller")
        
       
    }
    
    func getVendors(){
        var query:PFQuery = PFUser.query()!;
       

        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) users.")

                // Do something with the found objects
                if let objects = objects {
                    for object in objects{
                        //self.vendors.append(object["FoodTruckCompany"] as! String )
                        self.vendors.append(Vendor.init(name: object["FoodTruckCompany"] as! String, id: object.objectId!))
                        if(self.foodTruck.index(forKey: object.objectId!) == nil){
                            self.foodTruck[object.objectId!] = object as? PFUser
                        }
                    }


                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
            }

        }
    }
    
    
    func getEvents(location : PFGeoPoint) {
        
        
        let query = PFQuery(className:"Event")
        query.includeKey("FoodTruck")
        query.whereKey("geoPoint", nearGeoPoint: location, withinMiles: self.distance)
        query.whereKey("End", greaterThanOrEqualTo: Date() )
        query.whereKey("End", lessThanOrEqualTo: Date().nextWeek)
        query.addAscendingOrder("Start")
        
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) events.")
                self.stillfetching = true
                // Do something with the found objects
                if let objects = objects {
                    
                    for (index, object) in objects.enumerated() {
                        
                        let truck = object["FoodTruck"] as! PFUser
                        if(self.foodTruck.index(forKey: truck.objectId!) == nil){
                            self.foodTruck[truck.objectId!] = truck
                        }
                        if(index == objects.count - 1){
                            self.stillfetching = false
                            
                        }
                        self.addAnnotations(event: object)
                        
                    }
                    
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
            }
            
        }
    }
    
    func getCurrentLocation() {
        PFGeoPoint.geoPointForCurrentLocation {(geopoint, error) in
            if error == nil {
                self.getEvents(location: geopoint!)
                self.currentLocation = geopoint
                
            } else {
                // Handle with the error
                print("error getting current location for the user")
            }
        }
    }
    
    func addAnnotations(event: PFObject){
        
        let geoPoint = event["geoPoint"] as! PFGeoPoint
        let truck = event["FoodTruck"] as? PFObject
        
        
        let coord = CLLocation(latitude: geoPoint.latitude, longitude: geoPoint.longitude)
        let CLLCoordType = CLLocationCoordinate2D(latitude: coord.coordinate.latitude,
                                                  longitude: coord.coordinate.longitude);
        
        let startTime = event["Start"] as! Date
        let endTime = event["End"] as! Date
        
        
        let eventStart = startTime.toString(dateFormat: "h:mm a")
        let eventEnd = endTime.toString(dateFormat: "h:mm a")
        let eventStartEndTime = eventStart + " - " + eventEnd
        let index = self.mapAnnotations.count
        let anno = FoodTruckLocation(title: truck!["FoodTruckCompany"] as! String, subtitle: "", coordinate: CLLCoordType, image: truck!["Image"] as! PFFile, indexOfItem: index, companyID: (truck?.objectId)! , email: truck?["username"] as! String, date: startTime, eventStartEndTime : eventStartEndTime)
        
//        if(endTime >= Date() && endTime <= Date().endOfDay){
//                self.mapAnnotations[0].append(anno)
//        }
//        else if(endTime >= Date().tomorrow && endTime <= Date().tomorrow.endOfDay){
//                self.mapAnnotations[1].append(anno)
//        }
//        else{
//            self.mapAnnotations[2].append(anno)
//        }
        if(self.mapAnnotations.isEmpty){
            self.mapAnnotations.append([anno])
        }
        else{
            var addedAnno = false
            for (index, filter) in self.mapAnnotations.enumerated() {
                if(Calendar.current.isDate((mapAnnotations[index].first?.date)!, inSameDayAs: anno.date)){
                    mapAnnotations[index].append(anno)
                    addedAnno = true
                }
            }
            if(!addedAnno){
                 mapAnnotations.append([anno])
            }
            
        }
        
        
        
        
    }
    
    
    
    
}

extension Date {
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: startOfDay)!
    }
    var startOfDay: Date {
        return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)!
    }
    var endOfDay: Date {
        return Calendar.current.date(bySettingHour: 23, minute: 59, second: 0, of: self)!
    }
    var nextWeek: Date {
        return Calendar.current.date(byAdding: .day, value: 7, to: startOfDay)!
    }
    
    
}

extension Int {
    func getWeekDay() -> String {
       
        switch self {
        case  1:
            return "Sunday"
        case  2:
            return "Monday"
        case  3:
            return "Tuesday"
        case  4:
            return "Wednesday"
        case  5:
            return "Thursday"
        case  6:
            return "Friday"
        case  7:
            return "Saturday"
        default:
            return "Future"
        }
    }
    
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}

struct Vendor {
    var name : String
    var id : String
    
}



