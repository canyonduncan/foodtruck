//
//  WrapperTableViewCell.swift
//  foodTruck
//
//  Created by Sam Bryson on 5/8/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit

class WrapperTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        self.layer.cornerRadius = 12
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
