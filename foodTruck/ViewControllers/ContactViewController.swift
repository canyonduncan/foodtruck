//
//  ContactViewController.swift
//  foodTruck
//
//  Created by Sam Bryson on 3/4/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var contactHub: UIVisualEffectView!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    override func viewDidLoad() {
        contactHub.layer.cornerRadius = 15
        contactHub.layer.masksToBounds = false
        contactHub.clipsToBounds = true
    }
    
    @IBAction func closePopup(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func phoneTapped(_ sender: Any) {

        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
//                        controller.recipients = [(self.phone.titleLabel?.text)!]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else{
            print("Cannot Complete Call")
        }
    }
    
    @IBAction func twitterTapped(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "http://www.twitter.com")!)
    }
    
    @IBAction func emailTapped(_ sender: Any) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        //        composeVC.setToRecipients([titleLabel?.text as! String])
        composeVC.setSubject("Food Truck Finder")
        composeVC.setMessageBody("", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
}
