//
//  ViewCompanyVC.swift
//  foodTruck
//
//  Created by Canyon Duncan on 11/16/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import Foundation
import MessageUI
import ParseUI

class ViewCompanyVC: UIViewController {
    
    var stateController : StateController?
    var companyID : String?
    
    var imageView = UIImageView(frame: CGRect.zero)
    lazy var menu: Menu = {
        let m = Menu()
        m.viewCompanyVC = self
        m.stateController = self.stateController
        return m
    }()
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(self.view.subviews.count)
        print(self.view.subviews)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.backgroundColor = UIColor.clear
        self.view.addSubview(blurEffectView)
        self.view.sendSubview(toBack: blurEffectView)
        
        self.menu.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(menu)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.black
        self.view.addSubview(imageView)
        self.navBar.topItem?.title = self.stateController?.selectedTruck!["FoodTruckCompany"] as! String
        self.setupConstraints()
        self.setImage()
        
        
    }
    
    
    func setupConstraints(){
        let views = ["imageView": imageView,
                     "menu" : menu,
                     "navBar" : navBar
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-40-[navBar]-0-[imageView(300)]-0-[menu]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        
        let cvHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[menu]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += cvHorizontal
        
        let imageViewHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[imageView]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += imageViewHorizontal
        
        
        self.view.addConstraints(allConstraints)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setImage(){
        if var imageFile : PFFile = ( self.stateController?.selectedTruck!["Image"] as! PFFile) {
            imageFile.getDataInBackground(block: { (data, error) in
                if error == nil {
                    DispatchQueue.main.async {
                        // Async main thread

                        let image = UIImage(data: data!)
                        self.imageView.image = image
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        }

    }
    
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        
        composeVC.setSubject("Food Truck Finder")
        composeVC.setMessageBody("", isHTML: false)
        composeVC.setToRecipients([(self.stateController?.selectedTruck?["username"])! as! String])
        
       
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func sendMessage(){
        print("phoneButton")
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = [self.stateController?.selectedTruck!["phone"] as! String]
            
            controller.messageComposeDelegate = self
            //let appdelegate = UIApplication.shared.delegate as! AppDelegate
            //let viewcon = appdelegate.window?.rootViewController
            
            self.present(controller, animated: true, completion: nil)
            
        }
        else{
            print("Cannot Complete Call")
        }
    }
    
    func openFacebook() {
        print("facebookbutton")
//        UIApplication.shared.openURL(URL(string: "http://www.facebook.com/WorldsBestCornDogs/")!)
        let facebook =  stateController?.selectedTruck!["FacebookURL"] as? String
        let facebookurl = "http://\(facebook)"
        let safeURL = URL(string: "http://facebook.com")!
        
        guard let url = URL(string: facebookurl) else {
            return //be safe
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
        else{
            UIApplication.shared.open(safeURL, options: [:], completionHandler: nil)
        }
    }
    
}



extension ViewCompanyVC : MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        controller.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
}

