//
//  DetailImageCollectionCell.swift
//  foodTruck
//
//  Created by Sam Bryson on 3/5/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit

class DetailImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var detailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
        
        detailImageView.layer.cornerRadius = 10
        detailImageView.layer.masksToBounds = false
        
        // shadow
        
        detailImageView.layer.shadowColor = UIColor.black.cgColor
        detailImageView.layer.shadowOffset = CGSize(width: 3, height: 3)
        detailImageView.layer.shadowOpacity = 0.7
        detailImageView.layer.shadowRadius = 4.0
        
    }
    
    func setImage(user: PFObject){
        let file = user["Image"] as! PFFile
        file.getDataInBackground(block: { (data, error) -> Void in
            if let data = data {
                let image = UIImage(data: data)
                self.detailImageView.image = image
            }
        })
    }
    
}
