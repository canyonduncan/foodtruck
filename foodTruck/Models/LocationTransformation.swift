//
//  LocationTransformation.swift
//  ARound
//
//  Created by Sam Bryson on 12/4/17.
//  Copyright © 2017 Sam Bryson. All rights reserved.
//

import Foundation

struct LocationTranslation {
    var latitudeTranslation: Double
    var longitudeTranslation: Double
    var altitudeTranslation: Double
    init(latitudeTranslation: Double, longitudeTranslation: Double, altitudeTranslation: Double) {
        self.latitudeTranslation = latitudeTranslation
        self.longitudeTranslation = longitudeTranslation
        self.altitudeTranslation = altitudeTranslation
    }
}

