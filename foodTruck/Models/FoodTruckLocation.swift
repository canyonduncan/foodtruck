//
//  FoodTruckLocation.swift
//  foodTruck
//
//  Created by Canyon Duncan on 10/28/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class FoodTruckLocation: NSObject, MKAnnotation {
    
    let title: String?
    var subtitle: String?
    let coordinate: CLLocationCoordinate2D
    let image: PFFile?
    let indexOfItem: Int?
    let companyID: String
    let email: String
    let date: Date
    let eventStartEndTime : String?
    
  
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D, image: PFFile, indexOfItem: Int, companyID: String, email: String, date: Date, eventStartEndTime: String) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.image = image
        self.indexOfItem = indexOfItem
        self.companyID = companyID
        self.email = email
        self.date = date
        self.eventStartEndTime = eventStartEndTime
        
        
        super.init()
    }
    
    
    func setSubtitle(sub: String){
        self.subtitle = sub
    }
    // Annotation right callout accessory opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    
    func getWeekDay() {
        
    }
    
}
