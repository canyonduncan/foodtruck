//
//  LocationDistance.swift
//  ARound
//
//  Created by Sam Bryson on 12/4/17.
//  Copyright © 2017 Sam Bryson. All rights reserved.
//

import MapKit

extension CLLocationDistance {
    var distanceString: String {
        if self >= 1609.34 {
            return String(format: "%0.2f miles", self/1609.34)
        }
        return String(format: "%0.2f m", self)
    }
}
