//
//  CollectionFilterCell.swift
//  foodTruck
//
//  Created by Canyon Duncan on 8/6/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import Foundation
import UIKit
import ParseUI

class CollectionFilterCell: UICollectionViewCell {
    
    let label : UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.backgroundColor = UIColor.gray
        lb.text = "Today"
        //lb.layer.borderColor = UIColor.black.cgColor
        //lb.layer.borderWidth = 2
        lb.textAlignment = .center
        return lb
    }()
    
    override func awakeFromNib() {
        setupLayout()
        //self.layer.borderColor = UIColor.black as! CGColor
        //self.layer.borderWidth = 2
        
    }
    
    func setupLayout(){
        addSubview(label)
        
        
        let views = ["label" : label
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[label]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let labelHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[label]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += labelHorizontal
        
       
        
        
        addConstraints(allConstraints)
    }
    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
//        
//        return UIEdgeInsetsMake(0, 0, 0, 0)
//    }
   
        
}

