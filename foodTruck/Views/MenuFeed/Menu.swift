//
//  Menu.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/10/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit

class Menu: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.delegate = self
        cv.dataSource = self
        
        return cv
    }()
    
    lazy var menuBar: MenuBar = {
        let mb = MenuBar()
        mb.menu = self
        return mb
    }()
    
    let menuCell = "menuCell"
    let eventCell = "eventCell"
    let contactCell = "contactCell"
    
    var stateController : StateController?
    
    var viewCompanyVC : ViewCompanyVC?
    var insideMenuFeedCell: MenuFeedCell?
    var insideEventFeedCell: EventFeedCell?
    var insideContactFeedCell: ContactFeedCell?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        addSubview(menuBar)
        setupCollectionView()
        setupLayout()
        collectionView.register(EventFeedCell.self, forCellWithReuseIdentifier: eventCell)
        collectionView.register(MenuFeedCell.self, forCellWithReuseIdentifier: menuCell)
        collectionView.register(ContactFeedCell.self, forCellWithReuseIdentifier: contactCell)
        
        
    }
    
    func scrollToMenuIndex(menuIndex: Int){
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView.scrollToItem(at: indexPath, at: [], animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.item == 0){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: eventCell, for: indexPath) as! EventFeedCell
            cell.stateController = self.stateController
            cell.getEvents()
            self.insideEventFeedCell = cell
            return cell
        }
        else if(indexPath.item == 1){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCell, for: indexPath) as! MenuFeedCell
            cell.stateController = self.stateController
            cell.getMenuItems()
            self.insideMenuFeedCell = cell
            return cell
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: contactCell, for: indexPath) as! ContactFeedCell
            self.insideContactFeedCell = cell
            cell.stateController = self.stateController
            cell.viewCompanyVC = self.viewCompanyVC
            //cell.view
            cell.setupText()
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / 3
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = targetContentOffset.pointee.x / frame.width
        let indexPath = IndexPath(item: Int(index) , section: 0)
        
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCollectionView(){
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
            
        }
        
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)
    }
    
    func setupLayout(){
        let views = ["cv" : collectionView,
                     "menuBar": menuBar
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[menuBar(40)]-0-[cv]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let cvHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[cv]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += cvHorizontal
        
        let menuBarHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[menuBar]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += menuBarHorizontal
        
        addConstraints(allConstraints)
    }
    
    
}









