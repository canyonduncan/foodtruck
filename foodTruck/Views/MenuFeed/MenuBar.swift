//
//  MenuBar.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/9/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit

class MenuBar: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = UIColor.clear
        
        return cv
    }()
    
    let cellID = "cellID"
    let menuBarNames = ["Events", "Menu", "Contact"]
    var menu: Menu?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! MenuBarCell
        cell.label.text = menuBarNames[indexPath.row]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 3 , height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        let x = CGFloat(indexPath.row) * frame.width / 3
        //        horizontalBarLeftAnchorConstraint?.constant = x
        //
        //        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        //            self.layoutIfNeeded()
        //        }, completion: nil)
        
        menu?.scrollToMenuIndex(menuIndex: indexPath.item)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCollectionView()
        setupHorizontalBar()
    }
    
    func setupCollectionView() {
        collectionView.register(MenuBarCell.self, forCellWithReuseIdentifier: cellID)
        addSubview(collectionView)
        
        let selectedIndexPath = IndexPath(row: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
        
        var allConstraints = [NSLayoutConstraint]()
        let views = ["cv": collectionView
            ] as [String : Any]
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|[cv]|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let menuBarHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|[cv]|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += menuBarHorizontal
        
        
        
        addConstraints(allConstraints)
    }
    
    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    
    func setupHorizontalBar(){
        let horizontalBar = UIView()
        horizontalBar.backgroundColor = UIColor.red
        addSubview(horizontalBar)
        horizontalBar.translatesAutoresizingMaskIntoConstraints = false
        horizontalBarLeftAnchorConstraint = horizontalBar.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizontalBarLeftAnchorConstraint?.isActive = true
        horizontalBar.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        horizontalBar.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3, constant: 1).isActive = true
        horizontalBar.heightAnchor.constraint(equalToConstant: 4).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



class MenuBarCell: UICollectionViewCell{
    
    let label: UILabel = {
        let lb = UILabel()
        lb.text = "Menu"
        lb.textAlignment = .center
        lb.backgroundColor = UIColor(red: 247.00/255.00, green: 247.00/255.00, blue: 247.00/255.00, alpha: 1)
        
        lb.translatesAutoresizingMaskIntoConstraints = false
        
        return lb
    }()
    
    
    override var isHighlighted: Bool {
        didSet {
            label.textColor = isHighlighted ? UIColor.red : UIColor.black
        }
    }
    
    override var isSelected: Bool {
        didSet{
            label.textColor = isSelected ? UIColor.red : UIColor.black
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        
    }
    
    func setupViews(){
        backgroundColor = UIColor.clear
        addSubview(label)
        
        var allConstraints = [NSLayoutConstraint]()
        let views = ["lb": label
            ] as [String : Any]
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|[lb]|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let lbHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|[lb]|",
            options: [.alignAllCenterY],
            metrics: nil,
            views: views)
        allConstraints += lbHorizontal
        
        
        NSLayoutConstraint.activate(allConstraints)
        //addConstraints(allConstraints)
        
        
        
        
        
        
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}





