//
//  EventFeedCell.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/10/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse

class EventFeedCell: FeedCell {
    
    var events: [PFObject]?
    
    var viewCompanyVC : ViewCompanyVC?
    var stateController : StateController?
    
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    let fadeView:UIView = UIView()
    
    
    
    func getEvents() {
        print("inside get events")
        let objectID = self.stateController?.selectedTruck?.objectId
        let query = PFQuery(className:"Event")
        query.whereKey("FoodTruck", equalTo: PFObject(withoutDataWithClassName:"_User", objectId: objectID))
        query.cachePolicy = .cacheElseNetwork
        
        query.whereKey("End", greaterThan: Date())
        query.order(byAscending: "Start")
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) events.")
                if let objects = objects {
                    self.events = objects
                    
                    DispatchQueue.main.async {
                        
                        self.collectionView.reloadData()
                        self.activityView.stopAnimating()
                        self.fadeView.removeFromSuperview()
                        
                    }
                    
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                }
                
            }
        }
        
        
    }
    
    
    
    override func setupViews() {
        
        
        fadeView.frame = self.frame
        fadeView.backgroundColor = UIColor.clear
        //fadeView.alpha = 0.7
        
        addSubview(fadeView)
        
        fadeView.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = fadeView.center
        activityView.startAnimating()
        
        setupCV()
        setupLayout()
        
    }
    
    
    
    
    
    override func setupCV() {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .vertical
            //flowLayout.minimumLineSpacing = 0
        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(EventCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var numItems = 0
        if let count = self.events?.count {
            numItems = count
        }
        return numItems
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! EventCell
        
        cell.event = self.events?[indexPath.row]
        cell.setupCell()
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width - 20, height: 75)
    }
    
    
    
    
    
    
    func setupLayout(){
        let views = ["cv" : collectionView
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[cv]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        
        
        let cvHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[cv]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += cvHorizontal
        
        addConstraints(allConstraints)
    }
}



class EventCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        
    }
    
    var event : PFObject?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let locationLabel : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.boldSystemFont(ofSize: 16)
        lb.textColor = UIColor.black
        //lb.backgroundColor = UIColor.gray
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let dateLabel : UILabel = {
        let lb = UILabel()
        lb.text = "10/12/18 - 10:00 - 12:00"
        lb.font = UIFont.boldSystemFont(ofSize: 12)
        lb.textColor = UIColor.black
        //lb.backgroundColor = UIColor.gray
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    func setupViews(){
        
        backgroundColor = UIColor.white
        addSubview(dateLabel)
        addSubview(locationLabel)
        
        //self.layer.borderWidth = 1
        self.layer.cornerRadius = 15
        //self.layer.borderColor = UIColor.clear.cgColor
        
        setupLayout()
        
    }
    
    func setupCell(){
        if self.event != nil {
            self.dateLabel.text = (event!["Address"] as! String)
            let start = event!["Start"] as! Date
            let end = event!["End"] as! Date
            var dateString = ""
            
            if(Calendar.current.isDateInToday(start)){
                dateString = "Today"
            }
            else if(Calendar.current.isDateInTomorrow(start)){
                dateString = "Tomorrow"
                
            }
            else{
                dateString = start.toString(dateFormat: "MMM d")
            }
            self.locationLabel.text = " \(dateString), \(start.toString(dateFormat: "h:mm a")) - \(end.toString(dateFormat: "h:mm a"))"
            
        }
        
        
    }
    
    func setupLayout(){
        let views = ["locationlb" : locationLabel,
                     "datelb" : dateLabel
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[locationlb]-0-[datelb]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let locationlbHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-15-[locationlb]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += locationlbHorizontal
        
        let datelbHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-25-[datelb]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += datelbHorizontal
        
        
        
        addConstraints(allConstraints)
    }
}




