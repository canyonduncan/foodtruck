//
//  ContactFeedCell.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/11/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit
import MessageUI
import Parse

class ContactFeedCell: UICollectionViewCell {
    
    var stateController : StateController?
    var viewCompanyVC : ViewCompanyVC?

    
    let phone : UIButton = {
        let lb = UIButton()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.backgroundColor = UIColor.white
        lb.setTitleColor(UIColor.black, for: .normal)
        lb.layer.cornerRadius = 15
        
        //lb.textAlignment = .center
        return lb
    }()
    let email : UIButton = {
        let lb = UIButton()
        lb.backgroundColor = UIColor.white
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.setTitleColor(UIColor.black, for: .normal)
        lb.layer.cornerRadius = 15
        
        //lb.textAlignment = .center
        return lb
    }()

    let facebook : UIButton = {
        let lb = UIButton()
        lb.backgroundColor = UIColor.white
        //lb.textAlignment = .center
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.setTitleColor(UIColor.black, for: .normal)
        lb.addTarget(self, action:#selector(facebookButton), for: .touchUpInside)
        lb.layer.cornerRadius = 15
        return lb
    }()

    
    
    @objc func phoneButton(sender: UIButton) {
        self.viewCompanyVC?.sendMessage()
    }
    
    @objc func emailButton(sender: UIButton){
        print("email button")
        self.viewCompanyVC?.sendEmail()
        //self.present(composeVC, animated: true, completion: nil)
    }
    
    @objc func facebookButton(sender: UIButton) {
        self.viewCompanyVC?.openFacebook()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
        setupLayout()
        
        
        
    }

    
    func setupLayout(){

        addSubview(phone)
        addSubview(email)
        addSubview(facebook)
        

        let views = ["phone" : phone,
                     "email" : email,
                     "facebook" : facebook

            ] as [String : Any]

        var allConstraints = [NSLayoutConstraint]()

        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[phone(30)]-10-[email(30)]-10-[facebook(30)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints

        let phoneHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-30-[phone]-30-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += phoneHorizontal

        let emailHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-30-[email]-30-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += emailHorizontal

        let facebookHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-30-[facebook]-30-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += facebookHorizontal


        addConstraints(allConstraints)
    }

    func setupText() {
        
        self.email.setTitle(stateController?.selectedTruck!["username"] as? String, for: .normal)
        self.facebook.setTitle(stateController?.selectedTruck!["FacebookURL"] as? String, for: .normal)
        self.phone.setTitle(stateController?.selectedTruck!["phone"] as? String, for: .normal)
        facebook.addTarget(self, action:#selector(facebookButton), for: .touchUpInside)
        email.addTarget(self, action:#selector(emailButton), for: .touchUpInside)
        phone.addTarget(self, action:#selector(phoneButton) , for: .touchUpInside)


    }

    
    
   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}

extension ContactFeedCell : MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        controller.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
}











