//
//  CollectionCell.swift
//  foodTruck
//
//  Created by Canyon Duncan on 12/1/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import ParseUI

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!

    @IBOutlet weak var distanceLabel: UILabel!
    var companyID = String()
    var email = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
       
        blurView.layer.cornerRadius = 10
        blurView.layer.masksToBounds = false
        
//        label.layer.cornerRadius = 15
        label.clipsToBounds = true
        
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = false
        
        // border
        blurView.layer.borderWidth = 1.0
        blurView.layer.borderColor = UIColor.clear.cgColor
        
        // shadow
        blurView.layer.shadowColor = UIColor.black.cgColor
        blurView.layer.shadowOffset = CGSize(width: 3, height: 3)
        blurView.layer.shadowOpacity = 0.7
        blurView.layer.shadowRadius = 4.0
        
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOffset = CGSize(width: 3, height: 3)
        imageView.layer.shadowOpacity = 0.7
        imageView.layer.shadowRadius = 4.0
        
        distanceLabel.layer.borderColor = UIColor.clear.cgColor
        distanceLabel.layer.borderWidth = 1
        distanceLabel.layer.cornerRadius = 5
        //distanceLabel.layer.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).cgColor
        //distanceLabel.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        

//        print(companyID)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
}
