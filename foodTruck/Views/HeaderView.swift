//
//  CollectionReusableView.swift
//  foodTruck
//
//  Created by Canyon Duncan on 7/27/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import Foundation

class HeaderView : UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupLayout()
        self.backgroundColor = UIColor.clear
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    let titleLabel : UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.backgroundColor = UIColor.white
        lb.text = "Title"
        lb.textAlignment = .center
        lb.textColor = UIColor(red: 183/255.0, green: 16/255.0, blue: 27/255.0, alpha: 1)
        //lb.textColor = UIColor.black
        return lb
    }()
    
    let image : UIImageView = {
        let img = UIImageView(frame: CGRect.zero)
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "TruckAnno")
        img.layer.shadowColor = UIColor.black.cgColor
        img.layer.shadowOffset = CGSize(width: 3, height: 3)
        img.layer.shadowOpacity = 0.7
        img.layer.shadowRadius = 4.0
        return img
    }()
    
//    let blurView : UIVisualEffectView = {
//        let view = UIVisualEffectView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = UIColor.white
//        view.effect = UIBlurEffect.init(style: UIBlurEffectStyle.extraLight)
//        return view
//    }()
    
    
    func setupLayout(){
        
        addSubview(titleLabel)
        addSubview(image)
        //addSubview(blurView)
        
        
        
        let views = ["title" : titleLabel,
                     "img" : image
                     
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-25-[title]-20-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let verticalImgConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-25-[img(50)]|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalImgConstraints
        
        let titleHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-5-[img(50)]-30-[title]-10-|",
           // withVisualFormat: "H:|-0-[title]-0-|",
            
            options: [],
            metrics: nil,
            views: views)
        allConstraints += titleHorizontal
        
        
        
        addConstraints(allConstraints)
    }
    
//    func setupBlurLayout() {
//
//        blurView.contentView.addSubview(titleLabel)
//
//        let views = ["title" : titleLabel
//            ] as [String : Any]
//
//        var allConstraints = [NSLayoutConstraint]()
//
//        let verticalConstraints = NSLayoutConstraint.constraints(
//            withVisualFormat: "V:|-0-[title]-0-|",
//            options: [],
//            metrics: nil,
//            views: views)
//        allConstraints += verticalConstraints
//
//        let horizontalConstraints = NSLayoutConstraint.constraints(
//            withVisualFormat: "H:|-0-[title]-0-|",
//            options: [],
//            metrics: nil,
//            views: views)
//        allConstraints += horizontalConstraints
//
//
//
//
//
//        self.blurView.contentView.addConstraints(allConstraints)
//    }

    
    
}
