//
//  ApplicationService.swift
//  ARound
//
//  Created by Sam Bryson on 12/4/17.
//  Copyright © 2017 Sam Bryson. All rights reserved.
//

import Foundation

struct ApplicationServices {
    var navService: NavigationService
    var locationService: LocationService
}
