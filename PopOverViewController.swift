//
//  PopOverViewController.swift
//  foodTruck
//
//  Created by Sam Bryson on 3/2/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit

class PopOverViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var Popupview: UIView!
    
    @IBOutlet weak var tableView: UITableView!

    
    var companyName = String()
    var companyID = String()
    var email = String()
    private var finishedLoadingInitialTableCells = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        // Apply radius to Popupview
        Popupview.layer.cornerRadius = 10
        Popupview.layer.masksToBounds = true
        
    }
    
    
    // Returns count of items in tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.companyName.count;
    }
    
    
    //Assign values for tableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UITableViewCell
//
//        if let foodtruckcompany = object!["FoodTruckCompany"] {
//            cell.textLabel?.text = foodtruckcompany as! String
//            cell.companyID = (object?.objectId)!
//
//        }
//        cell.textLabel?.text = object!["FoodTruckCompany"] as! String
//        cell.companyID = (object?.objectId)!
//         cell.email = object!["email"] as! String
//
        return cell
    }
    
//    func queryForTable() -> PFQuery<PFObject> {
//        let query = PFQuery(className: "_User")
//
//        return query
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let indexPath = tableView.indexPathForSelectedRow!
//        let currentCell = tableView.cellForRow(at: indexPath)! // as! UITableViewCell
//        
//        self.companyName = (currentCell.textLabel?.text)!
////        self.companyID = (currentCell.companyID)
//        //self.email = (currentCell.email)
//        
//        performSegue(withIdentifier: "showViewCompany", sender: self)
//        
//        
//    }
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "showViewCompany", let destination = segue.destination as? ViewCompanyVC {
//            destination.companyName = self.companyName
//            destination.companyID = self.companyID
//            //destination.email = self.email
//            print("inside the segue")
//            print(destination.companyName)
//            
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        
//        var lastInitialDisplayableCell = false
//        
//        //change flag as soon as last displayable cell is being loaded (which will mean table has initially loaded)
//        if 3 > 0 && !finishedLoadingInitialTableCells {
//            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
//                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
//                lastInitialDisplayableCell = true
//            }
//        }
//        
//        if !finishedLoadingInitialTableCells {
//            
//            if lastInitialDisplayableCell {
//                finishedLoadingInitialTableCells = true
//            }
//            let rowHeight = cell.bounds.height
//            //animates the cell as it is being displayed for the first time
//            cell.transform = CGAffineTransform(translationX: 0, y: rowHeight/2)
//            cell.alpha = 0
//            
//            UIView.animate(withDuration: 0.5, delay: 0.05*Double(indexPath.row), options: [.curveEaseInOut], animations: {
//                cell.transform = CGAffineTransform(translationX: 0, y: 0)
//                cell.alpha = 1
//            }, completion: nil)
//        }
//    }

    
    // Close PopUp
    @IBAction func closePopup(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
}

