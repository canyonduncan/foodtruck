//
//  Mapable.swift
//  ARound
//
//  Created by Sam Bryson on 12/4/17.
//  Copyright © 2017 Sam Bryson. All rights reserved.
//

import MapKit

protocol Mapable: class  {
    var startingLocation: CLLocation! { get set }
    var mapView: MKMapView! { get set }
}


